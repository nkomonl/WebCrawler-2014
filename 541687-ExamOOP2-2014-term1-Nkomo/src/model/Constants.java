package model;

public class Constants {
	
	
	public static final int MAX_NUMBER_OF_SEEKER_THREADS = 200; // Should be adjustable in the GUI, preferably while running
	public static final String WORK_AT_HAND_FILENAME ="Database/WorkAtHand.txt"; // Should be changeable through the GUI
			//"C:\\Database\\WorkAtHand.txt"; // Should be changeable through the GUI
	public static final String VERTICES_FILENAME = "Database/Vertices.txt"; // Should be changeable through the GUI
	public static final String EDGES_FILENAME = "Database/Edges.txt"; // Should be changeable through the GUI
	public static final String DEFAULT_START = "http://www.nujij.nl"; 
	}
