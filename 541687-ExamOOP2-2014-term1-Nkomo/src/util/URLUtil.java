package util;

import org.jsoup.Connection;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.UnsupportedMimeTypeException;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.SortedMap;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Pattern;

/**
 * An utility class used to process URL's.
 *
 * @author harald.drillenburg
 */

public class URLUtil {
	// https://code.google.com/p/crawler4j
	//open source crawler tutorial
	private static Pattern protocolPattern = Pattern.compile("://");

	/**
	 * This method is used to normalize a URL. In order to do this, the
	 * following steps are taken: - Strip all parameters from the URL - Strip
	 * signs like ; and # at the end of the URL - Strip all file extensions -
	 * Strip any remaing / sign at the end of the URL - Make sure the remaining
	 * URL does not start or end with a space
	 *
	 * @param name
	 *            the String representation of the URL
	 * @return String the stripped URL
	 */
	public static String stripURL(String url) {
		URL canonicalURL = getCanonicalURL(url, null);
		if (canonicalURL != null) {
			return canonicalURL.toExternalForm();
		}
		return null;
	}

	public static URL getCanonicalURL(String href, String context) {

		try {

			URL canonicalURL;
			if (context == null) {
				canonicalURL = new URL(href);
			} else {
				canonicalURL = new URL(new URL(context), href);
			}

			String path = canonicalURL.getPath();

			/*
			 * Normalize: no empty segments (i.e., "//"), no segments equal to
			 * ".", and no segments equal to ".." that are preceded by a segment
			 * not equal to "..".
			 */
			path = new URI(path).normalize().toString();

			/*
			 * Convert '//' -> '/'
			 */
			int idx = path.indexOf("//");
			while (idx >= 0) {
				path = path.replace("//", "/");
				idx = path.indexOf("//");
			}

			/*
			 * Drop starting '/../'
			 */
			while (path.startsWith("/../")) {
				path = path.substring(3);
			}

			/*
			 * Trim
			 */
			path = path.trim();

			final SortedMap<String, String> params = createParameterMap(canonicalURL.getQuery());
			final String queryString;

			if (params != null && params.size() > 0) {
				String canonicalParams = canonicalize(params);
				queryString = (canonicalParams.isEmpty() ? "" : "?" + canonicalParams);
			} else {
				queryString = "";
			}

			/*
			 * Add starting slash if needed
			 */
			if (path.length() == 0) {
				path = "/" + path;
			}

			/*
			 * Drop default port: example.com:80 -> example.com
			 */
			int port = canonicalURL.getPort();
			if (port == canonicalURL.getDefaultPort()) {
				port = -1;
			}

			/*
			 * Lowercasing protocol and host
			 */
			String protocol = canonicalURL.getProtocol().toLowerCase();
			String host = canonicalURL.getHost().toLowerCase();
			String pathAndQueryString = normalizePath(path) + queryString;

			return new URL(protocol, host, port, pathAndQueryString);

		} catch (MalformedURLException ex) {
			return null;
		} catch (URISyntaxException ex) {
			return null;
		}
		// }

	}

	/**
	 * Takes a query string, separates the constituent name-value pairs, and
	 * stores them in a SortedMap ordered by lexicographical order.
	 * 
	 * @return Null if there is no query string.
	 */
	private static SortedMap<String, String> createParameterMap(final String queryString) {
		if (queryString == null || queryString.isEmpty()) {
			return null;
		}

		final String[] pairs = queryString.split("&");
		final Map<String, String> params = new HashMap<String, String>(pairs.length);

		for (final String pair : pairs) {
			if (pair.length() == 0) {
				continue;
			}

			String[] tokens = pair.split("=", 2);
			switch (tokens.length) {
			case 1:
				if (pair.charAt(0) == '=') {
					params.put("", tokens[0]);
				} else {
					params.put(tokens[0], "");
				}
				break;
			case 2:
				params.put(tokens[0], tokens[1]);
				break;
			}
		}
		return new TreeMap<String, String>(params);
	}

	/**
	 * Canonicalize the query string.
	 * 
	 * @param sortedParamMap
	 *            Parameter name-value pairs in lexicographical order.
	 * @return Canonical form of query string.
	 */
	private static String canonicalize(final SortedMap<String, String> sortedParamMap) {
		if (sortedParamMap == null || sortedParamMap.isEmpty()) {
			return "";
		}

		final StringBuffer sb = new StringBuffer(100);
		for (Map.Entry<String, String> pair : sortedParamMap.entrySet()) {
			final String key = pair.getKey().toLowerCase();
			if (key.equals("jsessionid") || key.equals("phpsessid") || key.equals("aspsessionid")) {
				continue;
			}
			if (sb.length() > 0) {
				sb.append('&');
			}
			sb.append(percentEncodeRfc3986(pair.getKey()));
			if (!pair.getValue().isEmpty()) {
				sb.append('=');
				sb.append(percentEncodeRfc3986(pair.getValue()));
			}
		}
		return sb.toString();
	}

	/**
	 * Percent-encode values according the RFC 3986. The built-in Java
	 * URLEncoder does not encode according to the RFC, so we make the extra
	 * replacements.
	 * 
	 * @param string
	 *            Decoded string.
	 * @return Encoded string per RFC 3986.
	 */
	private static String percentEncodeRfc3986(String string) {
		try {
			string = string.replace("+", "%2B");
			string = URLDecoder.decode(string, "UTF-8");
			string = URLEncoder.encode(string, "UTF-8");
			return string.replace("+", "%20").replace("*", "%2A").replace("%7E", "~");
		} catch (Exception e) {
			return string;
		}
	}

	private static String normalizePath(final String path) {
		return path.replace("%7E", "~").replace(" ", "%20");
	}

	/**
	 * Checks whether a given URL is actually reachable (i.e. whether a
	 * connection can be established) If an URL could not be reached, just
	 * return false, do not throw any Exception here
	 *
	 * @param anURL
	 *            anURL the String representation of the URL to check for
	 * @return boolean true if a connection could be established, false
	 *         otherwise
	 */
//============================================================================================
	//============================================================================================	
	/*
	 * 
	 * 
	 * 100 => 'Continue', 101 => 'Switching Protocols', 200 => 'OK', 201 =>
	 * 'Created', 202 => 'Accepted', 203 => 'Non-Authoritative Information', 204
	 * => 'No Content', 205 => 'Reset Content', 206 => 'Partial Content', 301 =>
	 * 'Moved Permanently', 302 => 'Found', 303 => 'See Other', 304 => 'Not
	 * Modified', 305 => 'Use Proxy', 307 => 'Temporary Redirect', 400 => 'Bad
	 * Request', 401 => 'Unauthorized', 403 => 'Forbidden', 404 => 'Not Found',
	 * 405 => 'Method Not Allowed', 406 => 'Not Acceptable', 407 => 'Proxy
	 * Authentication Required', 408 => 'Request Timeout', 409 => 'Conflict',
	 * 410 => 'Gone', 411 => 'Length Required', 412 => 'Precondition Failed',
	 * 413 => 'Request Entity Too Large', 414 => 'Request-URI Too Long', 415 =>
	 * 'Unsupported Media Type', 416 => 'Request Range Not Satisfiable', 417 =>
	 * 'Expectation Failed', 422 => 'Unprocessable Entity', 500 => 'Internal
	 * Server Error', 501 => 'Not Implemented', 502 => 'Bad Gateway', 503 =>
	 * 'Service Unavailable', 504 => 'Gateway Timeout', 505 => 'HTTP Version Not
	 * Supported',
	 */

	private static boolean isReachableJSoup(Connection connection, String url) {

		int response = 400;
		try {
			response = connection.execute().statusCode();

		} catch (IOException | IllegalArgumentException e) {
			e.printStackTrace();
			System.out.println(url + "method has been passed an illegal or inappropriate argument");
			if (e.getClass().toString().equals("class java.net.SocketException")) {
				System.out.println(" there is an error creating or accessing a Socket." + url);
			}
			if (e.getClass().toString().equals("class org.jsoup.HttpStatusException")) {
				HttpStatusException exception = (HttpStatusException) e;

				System.out.println(" HTTP request resulted in a not OK HTTP response." + exception.getStatusCode());
			}
			return false;
		}
		if (200 <= response && response <= 399) {
			return true;
		} else {
			System.out.println(url + "Ristriction response....above 400...as " + response);
			return false;
		}

	}

	/**
	 * This method gets a webpage in the form of a String and retrieves all
	 * anchors in it. Remember, an anchor is of the form <a href= , and what
	 * counts is the URL in the quote following. If anything goes wrong reading
	 * the URL itself: see comments in the source code given
	 *
	 * @param anURL
	 *            anURL the String representation of the URL to be retrieved //
	 *            * @param callback callback the object to tell how many bytes
	 *            were retrieved
	 * @return A List containing the anchors found
	 */

	public static ArrayList<String> getStops(String anURL, StatisticsCallback callback) throws IOException {
		ArrayList<String> result = new ArrayList<>();
		long bytes = 0;
		try {

		
			Connection connection = Jsoup
					.connect(anURL)
			
					.userAgent(
							"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21")
	
					.timeout(10000)
					
					.referrer("http://www.google.com");

			
			if (isReachableJSoup(connection, anURL)) {
				Document document = connection.get();

			
				bytes += document.toString().getBytes().length;


				Elements elements = document.select("a[href]");
				elements.addAll(document.select("frame[src]"));

				for (Element element : elements) {
					if (element.attr("abs:href") != null)
						result.add(element.attr("abs:href"));
					if (element.attr("abs:src") != null)
						result.add(element.attr("abs:src"));
				}
			}
			
		} catch (UnsupportedMimeTypeException e) {
			System.out.println("Can't parse " + e.getMimeType());
		}

		
		callback.amountUsed(bytes);
		return result;
	}
}
