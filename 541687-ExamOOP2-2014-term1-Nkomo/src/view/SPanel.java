package view;

import javax.swing.JPanel;

import java.awt.AlphaComposite;
import java.awt.Color;

import javax.swing.ImageIcon;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JLabel;

import model.Constants;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class SPanel extends JPanel {
	/**
	 * 
	 */
	//private static final long serialVersionUID = 1L;
	private JTextField textField_4;
	private String website;
	private JButton startButtonGui1;
	private JButton cancelButtonGui1;
	private JButton btnClearEntry;
	private JButton btnClearEntry2;
	private JLabel pictureLabel;
	
	int numberThreads;
	private JTextField textField_5;
	String ff;
	String df;

	public SPanel(final XXFrame xxFrame) {

		//setForeground(Color.WHITE);
		
		
		textField_4 = new JTextField();
		textField_4.setText("200");
		textField_5 = new JTextField();
		textField_5.setText("http://www.nujij.nl");
		// ================================================================================
		startButtonGui1 = new JButton("Start");
		startButtonGui1.setBounds(166, 310, 74, 25);
		add(startButtonGui1);
		startButtonGui1.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				entryCheck();

				xxFrame.switchDisplay();
			}

		});

		// ================================================================================
		cancelButtonGui1 = new JButton("Cancel");
		cancelButtonGui1.setBounds(361, 310, 90, 25);
		add(cancelButtonGui1);
		// ==================================================================================
		cancelButtonGui1.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}

		});
		// ==================================================================================
		btnClearEntry = new JButton("Clear Entry");
		btnClearEntry.setBounds(153, 359, 117, 25);
		add(btnClearEntry);
		btnClearEntry2 = new JButton("Restore Default");
		btnClearEntry2.setBounds(329, 359, 163, 25);
		add(btnClearEntry2);

		// ====================================================================================

		btnClearEntry.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				textField_4.setText("4");
				textField_5.setText("http://www.");
				// System.out.print("TEST cleared..........XX.");

			}

		});

		btnClearEntry2.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				textField_4.setText("200");
				textField_5.setText("http://www.nujij.nl");
				// System.out.print("setup restored..........XX.");

			}

		});

		
		
		
		// ========================================================================================
		// create objects first so that we see it,then call setup
		setupPanel();
	}

	protected void entryCheck() {

//		if (Integer.parseInt(textField_4.getText()) < 1) {
//			System.out.print("TEST NEGATIVE..below minimum number of threads.");
//			textField_4.setText("4 is minimum");
//			numberThreads =4;
//
//		}
//
//		else {
			try {
				if ((Integer.parseInt(textField_4.getText()) > 3)&&(Integer.parseInt(textField_4.getText()) < 200)) {
					numberThreads = Integer.parseInt(textField_4.getText());
					System.out.println("Number of threads are : "
							+ numberThreads);
				} 
					if (Integer.parseInt(textField_4.getText()) < 4){
					numberThreads = 4;
					System.out.println("Number of threads are at Default minimum of 4");
						}
				
				//TextField.getText().equals("")
				if (Integer.parseInt(textField_4.getText()) > 200) {
					//numberThreads = Integer.parseInt(textField_4.getText());
					System.out.println("Number of threads above 200 not allowed. . . default maximum 200 selected"
					);
					numberThreads=Constants.MAX_NUMBER_OF_SEEKER_THREADS;
				} 
				
				if ((textField_5.getText().equals(""))||(textField_5.getText().equals("http://www."))) {
					System.out.print("TEST blank.");
					//textField_5.setText("can not be blank...Default selected as www.nujij.nl");
					website = ("");
					System.out.println("Starting to crawl blank website input ...Default selected as www.nujij.nl");
					// ==================================================>
					WebCrawlerMain.givenNumber = numberThreads;
					WebCrawlerMain.selectedWebsite=website;
					XXFrame.screenNumber = 2;
				}

				else {
					try {
						website = textField_5.getText();
						System.out.println("Starting to crawl on the website: " + website);
						// ==================================================>
						WebCrawlerMain.givenNumber = numberThreads;
						WebCrawlerMain.selectedWebsite=website;
						XXFrame.screenNumber = 2;
					} catch (NumberFormatException e) {
						System.out.println("Not a website." + website);
						textField_5.setText("http://www.nujij.nl");
					}
				}

			} catch (NumberFormatException e) {
				System.out.println("Not a integer." + numberThreads);
				textField_4.setText("enter  integer amount");
			}
		}
	//}

	private void setupPanel() {
		this.setBackground(Color.LIGHT_GRAY);
		setLayout(null);

		textField_4.setBounds(410, 170, 209, 19);
		add(textField_4);
		textField_4.setColumns(10);

		textField_5.setBounds(410, 219, 209, 19);
		add(textField_5);
		textField_5.setColumns(10);

		JLabel lblEv = new JLabel(
				"Welcome to the webcrawler program. To begin, please enter the number of threads to ");
		lblEv.setBounds(39, 90, 649, 36);
		add(lblEv);

		JLabel lblthreads = new JLabel(
				"Number of threads to run.(Max200-Min 4)");
		lblthreads.setBounds(39, 169, 360, 19);
		add(lblthreads);

		JLabel lblBandwidth = new JLabel(
				"Initial website(Enter format=> http://www.nujij.nl)");
		lblBandwidth.setBounds(39, 222, 360, 15);
		add(lblBandwidth);

		JLabel lblRunAndThe = new JLabel(
				"run and the initial website to crawl in the form below.");
		lblRunAndThe.setBounds(39, 105, 649, 36);
		add(lblRunAndThe);
		
		pictureLabel = new JLabel("New label");
		pictureLabel.setIcon(new ImageIcon("images/im4x.jpg"));
		pictureLabel.setBounds(0, 0, 702, 550);
	
		add(pictureLabel);

	}
}
