package view;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.HashSet;
import java.util.SortedSet;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import model.Edge;
import model.Graph;
import model.Vertex;
import util.MemoryUtil;
import util.TablePacker;

import java.awt.Component;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.border.LineBorder;

import java.awt.Color;

class StatisticsPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	// / Components to build the table
	private JTable vertexList;
	private DefaultTableModel model; // / Model for the table. Using
										// DefaultTableModel so we don't have to
										// implement an abstract table..
	private JTableHeader columnName; // / Header of the table (column names)

	// // TextFields for Statistics overview
	static JTextField vertiPoints;
	private JTextField edges;
	private String edgeOverVerti; // // Edges divided by vertiPoints. Needs a
									// better name!
	private JTextField edgeOverVertiTextfield;
	private JTextField threads;
	private JTextField bandwidth;

	// / Panels and their holders.
	private JPanel panel4buttons; // / Panel to hold all the buttons. Positioned
									// at bottom.
	private JScrollPane mainScroller; // / ScrollPane to hold the table of data.
	private JPanel displayPanel; // / Panel to hold statistics
	private JPanel scrollPaneHolder; // / Panel to hold mainScroller
	private JButton stopButton;
	private JButton RefreshButton;

	public StatisticsPanel() {

		// / Create the components (call the proper methods)
		// createButtons();

		panel4buttons = new JPanel();
		panel4buttons.setBounds(0, 440, 707, 110);
		panel4buttons.setVisible(true);
		/*
		 * ButtonUtils.addButton(panel4buttons, "Start", new ActionListener() {
		 * 
		 * @Override public void actionPerformed(ActionEvent e) { refresh(); }
		 * });
		 */

		// *****************************************************************************************************
		// createTable();

		vertexList = new JTable(new DefaultTableModel());
		vertexList.setToolTipText("");
		vertexList.setFillsViewportHeight(true);
		vertexList.setBorder(new LineBorder(new Color(0, 0, 0), 0));
		model = (DefaultTableModel) vertexList.getModel();
		vertexList.setVisible(true);
//==============>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	//fix table column size 
		vertexList.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);


		
		
		for (int column = 0; column < vertexList.getColumnCount(); column++) {
			TableColumn tableColumn = vertexList.getColumnModel().getColumn(column);
			int preferredWidth = tableColumn.getMinWidth();
			int maxWidth = tableColumn.getMaxWidth();

			for (int row = 0; row < vertexList.getRowCount(); row++) {
				TableCellRenderer cellRenderer = vertexList.getCellRenderer(row, column);
				Component c = vertexList.prepareRenderer(cellRenderer, row, column);
				int width = c.getPreferredSize().width + vertexList.getIntercellSpacing().width;
				preferredWidth = Math.max(preferredWidth, width);

				// We've exceeded the maximum width, no need to check other rows

				if (preferredWidth >= maxWidth) {
				preferredWidth = maxWidth;
				
					break;
				}
				else{
			//====================>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
					preferredWidth = 50;
				break;
				}
			}
			// to fix table column width   add 2
			tableColumn.setMinWidth(2);
			tableColumn.setMaxWidth(preferredWidth);
//			tableColumn.setMaxWidth(50);
			//========================================================>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//			tableColumn.setPreferredWidth(preferredWidth);
		}

		// / Column names (first row) find better implementation and try with
		// scrollpane.
		model.addColumn("#");
		model.addColumn("Degree");
		model.addColumn("Uniform Resource Locator");

		// / Get columnName so we can set column names to visible
		columnName = vertexList.getTableHeader();
		columnName.setVisible(true);

		// *****************************************************************************************************
		// createScrollPanePanel();
		// *****************************************************************************************************
		mainScroller = new JScrollPane();
		scrollPaneHolder = new JPanel();
		scrollPaneHolder.setBorder(new LineBorder(new Color(0, 0, 0)));
		scrollPaneHolder.setBounds(122, 125, 467, 313);

		scrollPaneHolder.setLayout(new BorderLayout());
		scrollPaneHolder.add(columnName, BorderLayout.NORTH);
		scrollPaneHolder.add(mainScroller, BorderLayout.CENTER);

		mainScroller.add(vertexList); // / Add the table to the scrollable pane
		mainScroller.setViewportView(vertexList);

		// createTextFields();
		// *****************************************************************************************************

		vertiPoints = new JTextField(String.valueOf(WebCrawlerMain.internetModel.getNumberOfVertices()));
		edges = new JTextField(String.valueOf(WebCrawlerMain.internetModel.getNumberOfEdges()));
		if (WebCrawlerMain.initializerX == true) {

			edgeOverVerti = String.valueOf(WebCrawlerMain.internetModel.getNumberOfEdges()
					/ WebCrawlerMain.internetModel.getNumberOfVertices());
		}
		;

		if (WebCrawlerMain.initializerX == false) {
			edgeOverVerti = String.valueOf(WebCrawlerMain.internetModel.getNumberOfEdges() / 1);
			WebCrawlerMain.initializerX = true;

		}
		;

		edgeOverVertiTextfield = new JTextField(edgeOverVerti);
		bandwidth = new JTextField(MemoryUtil.readableFileSize((WebCrawlerMain.internetModel.getBandwidthUsed())));

		// createStatsPanel();

		// *****************************************************************************************************
		// Create vertex table. Create Statistics Overview panel.
		// Use a Grid Layout to put the Statistics Overview on. I used
		// 5 by 5 to have some layouting.

		displayPanel = new JPanel();
		displayPanel.setBounds(0, 0, 707, 120);
		displayPanel.setVisible(true);
		setLayout(null);

		this.add(displayPanel, BorderLayout.NORTH);
		GridBagLayout gbl_displayPanel = new GridBagLayout();
		gbl_displayPanel.columnWidths = new int[] { 226, 82, 81, 0 };
		gbl_displayPanel.rowHeights = new int[] { 20, 20, 20, 20, 20, 0 };
		gbl_displayPanel.columnWeights = new double[] { 0.0, 0.0, 0.0, Double.MIN_VALUE };
		gbl_displayPanel.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		displayPanel.setLayout(gbl_displayPanel);

		// // Add new labels and the textfields to the panel
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.BOTH;
		gbc.insets = new Insets(0, 0, 5, 5);
		gbc.gridx = 1;
		gbc.gridy = 0;
		JLabel label_2 = new JLabel("# Vertices");
		displayPanel.add(label_2, gbc);

		// / TextFields for OverView, replace Strings with appropriate getX()
		// methods
		vertiPoints = new JTextField(String.valueOf(WebCrawlerMain.vertiTotal));
		GridBagConstraints gbc_vertiPoints = new GridBagConstraints();
		gbc_vertiPoints.fill = GridBagConstraints.BOTH;
		gbc_vertiPoints.insets = new Insets(0, 0, 5, 0);
		gbc_vertiPoints.gridx = 2;
		gbc_vertiPoints.gridy = 0;
		displayPanel.add(vertiPoints, gbc_vertiPoints);
		GridBagConstraints gbc_1 = new GridBagConstraints();
		gbc_1.fill = GridBagConstraints.BOTH;
		gbc_1.insets = new Insets(0, 0, 5, 5);
		gbc_1.gridx = 1;
		gbc_1.gridy = 1;
		JLabel label_4 = new JLabel("# Edges");
		displayPanel.add(label_4, gbc_1);
		edges = new JTextField(String.valueOf(WebCrawlerMain.edgesTotal));
		GridBagConstraints gbc_edges = new GridBagConstraints();
		gbc_edges.fill = GridBagConstraints.BOTH;
		gbc_edges.insets = new Insets(0, 0, 5, 0);
		gbc_edges.gridx = 2;
		gbc_edges.gridy = 1;
		displayPanel.add(edges, gbc_edges);
		GridBagConstraints gbc_2 = new GridBagConstraints();
		gbc_2.fill = GridBagConstraints.BOTH;
		gbc_2.insets = new Insets(0, 0, 5, 5);
		gbc_2.gridx = 1;
		gbc_2.gridy = 2;
		JLabel label = new JLabel("Edge / Vertice");
		displayPanel.add(label, gbc_2);
		edgeOverVertiTextfield = new JTextField(String.valueOf(WebCrawlerMain.ratio));
		GridBagConstraints gbc_edgeOverVertiTextfield = new GridBagConstraints();
		gbc_edgeOverVertiTextfield.fill = GridBagConstraints.BOTH;
		gbc_edgeOverVertiTextfield.insets = new Insets(0, 0, 5, 0);
		gbc_edgeOverVertiTextfield.gridx = 2;
		gbc_edgeOverVertiTextfield.gridy = 2;
		displayPanel.add(edgeOverVertiTextfield, gbc_edgeOverVertiTextfield);
		GridBagConstraints gbc_3 = new GridBagConstraints();
		gbc_3.fill = GridBagConstraints.BOTH;
		gbc_3.insets = new Insets(0, 0, 5, 5);
		gbc_3.gridx = 1;
		gbc_3.gridy = 3;
		JLabel label_3 = new JLabel("# Threads");
		displayPanel.add(label_3, gbc_3);
		threads = new JTextField(String.valueOf(Thread.activeCount()));
		GridBagConstraints gbc_threads = new GridBagConstraints();
		gbc_threads.fill = GridBagConstraints.BOTH;
		gbc_threads.insets = new Insets(0, 0, 5, 0);
		gbc_threads.gridx = 2;
		gbc_threads.gridy = 3;
		displayPanel.add(threads, gbc_threads);
		GridBagConstraints gbc_4 = new GridBagConstraints();
		gbc_4.fill = GridBagConstraints.BOTH;
		gbc_4.insets = new Insets(0, 0, 0, 5);
		gbc_4.gridx = 1;
		gbc_4.gridy = 4;
		JLabel label_1 = new JLabel("Bandwidth");
		displayPanel.add(label_1, gbc_4);

		// Create vertex table. Create Statistics Overview panel.

		// *****************************************************************************

		// / Position the components accordingly on the main frame
		this.add(displayPanel, BorderLayout.NORTH); // / Add the displayPanel to
		bandwidth = new JTextField(MemoryUtil.readableFileSize(WebCrawlerMain.internetModel.getBandwidthUsed()));
		GridBagConstraints gbc_bandwidth = new GridBagConstraints();
		gbc_bandwidth.fill = GridBagConstraints.BOTH;
		gbc_bandwidth.gridx = 2;
		gbc_bandwidth.gridy = 4;
		displayPanel.add(bandwidth, gbc_bandwidth);
		// the top of the frame
		this.add(scrollPaneHolder); // / Add the
									// ScrollPaneHolder
									// (table) to the
									// center of the
									// frame
		this.add(panel4buttons); // / Add the buttons at the
		panel4buttons.setLayout(null);
		stopButton = new JButton("stop");
		stopButton.setBounds(230, 25, 86, 25);
		panel4buttons.add(stopButton);
		RefreshButton = new JButton("refresh");
		RefreshButton.setBounds(359, 25, 97, 25);
		panel4buttons.add(RefreshButton);
		RefreshButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					//==========>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
					
										model.setRowCount(0);
				
					
					//WebCrawlerMain.startCrawling();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		stopButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					WebCrawlerMain.stopCrawling();

					WebCrawlerMain.executor.shutdown();// make more variables
														// protected
					WebCrawlerMain.stopRequest();
					WebCrawlerMain.internetModel.writeGraph();
					WebCrawlerMain.databaseThread.writeAllWorkAtHand();
				} catch (FileNotFoundException e1) {
					System.out.println(" ****************** Brute CLOSE  **********************   ");
					System.exit(0);
					WebCrawlerMain.executor.shutdownNow();

				} catch (UnsupportedEncodingException e1) {
					e1.printStackTrace();
				}

			}
		});
		// bottom of the frame
	}

	// / Refresh UI (table)
	public void refresh() {
		// Refresh the content of the table. The call the TablePacker.
		new TablePacker(TablePacker.VISIBLE_ROWS, true).pack(vertexList);
		WebCrawlerMain.copyInfo();
		for (Vertex vertex : WebCrawlerMain.Vertices) {
			String[] row = { Integer.toString(vertex.getNumberOfTargetedBys()),
					Integer.toString(vertex.getNumberOfEdges()), vertex.getName() };
			model.addRow(row);
			model.fireTableDataChanged(); // Update the table!
			
		}
		new TablePacker(TablePacker.VISIBLE_ROWS, true).pack(vertexList);

		refreshUp();

	}

	public void refreshUp() {
		
		ThreadGroup currentGroup = Thread.currentThread().getThreadGroup();
		int noThreads = currentGroup.activeCount();

		WebCrawlerMain.copyTopInfo();
		vertiPoints.setText(WebCrawlerMain.vertiTotal);
		edges.setText(WebCrawlerMain.edgesTotal);
		edgeOverVertiTextfield.setText(WebCrawlerMain.ratio);
		threads.setText(String.valueOf(noThreads));
		bandwidth.setText(WebCrawlerMain.memUsage);

	}

}