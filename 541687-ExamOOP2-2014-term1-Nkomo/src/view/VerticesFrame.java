package view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.TimerTask;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

//import view.WebCrawlerMain.StatisticsPanel;

class VerticesFrame extends JFrame {
	// A JFrame used to host the Statistics Panel//variables

	// private JLabel item1;

	private static final long serialVersionUID = 1L;

	public VerticesFrame() {

		// Create the frame, add the right panel.
		/**(ADD CORRECT PANEL OR ADD THE WEST SIDE PANEL??)*/
		//Use a BorderLayout. Set
		// the title.
		super("Web Statistics");
		this.setSize(new Dimension(710, 559));
		this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		getContentPane().setLayout(new BorderLayout());

		// The following code can be kept here. It makes sure refresh is
		// called every three seconds.
		// final or we get error asking for static in timer
		final StatisticsPanel refresher = new StatisticsPanel();
		getContentPane().add(refresher, BorderLayout.CENTER);
		java.util.Timer timer = new java.util.Timer();
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				refresher.refresh();// Insert a call to the refresh method here. Make sure to do the casting.
			}
		}, 500, 500);
	}
}

