package view;

import controller.DatabaseThread;
import controller.EdgeSeeker;
import model.Constants;
import model.Graph;
import model.Vertex;
import util.MemoryUtil;

import java.awt.*;
import java.io.*;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * This class is the main Swing class building the GUI Defined are the
 * WebCrawlerMain, the frame and the panel classes Also shown are several
 * methods you might need The implementation you'll have to do yourself A menu
 * is optional
 * 
 *
 * @author harald.drillenburg
 */

public class WebCrawlerMain extends StatisticsPanel {
	
	private static final long serialVersionUID = 1L;
	/**
	 * if (givenNumber>Constants.MAX_NUMBER_OF_SEEKER_THREADS){ givenNumber=
	 * 200;}else{numberOfThreads=givenNumber; protected static ExecutorService
	 * executor = Executors.newFixedThreadPool(numberOfThreads); }
	 * 
	 * 
	 */
	public static int givenNumber;
	public static int numberOfThreads;
	protected static ExecutorService executor = Executors.newFixedThreadPool(Constants.MAX_NUMBER_OF_SEEKER_THREADS);
	// Here, define some nice way of using a thread pool
	public static Graph internetModel = new Graph();
	protected static DatabaseThread databaseThread = new DatabaseThread(
			internetModel);
	public static final int DEFAULT_WIDTH = 700;
	public static final int DEFAULT_HEIGHT = 550;
	public static SortedSet<Vertex> Vertices = new TreeSet<>();
	protected static String vertiTotal;
	protected static String edgesTotal;
	protected static String ratio;
	protected static String memUsage;
	protected static boolean stopRequested = false;
	protected static boolean initializerX = false;
	protected static XXFrame Framex2;
	protected static String selectedWebsite;

	public static void main(String[] args) {
		try {

			Framex2 = new XXFrame();
			Framex2.start();

			// new WebCrawlerMain();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/************************
 */
	public WebCrawlerMain() throws Exception {
		// Here, create a new Runnable to insert into the EventQueue
		// The Runnable should create the actual frame and set it to visible
		//stopXXframe();
	
		if (givenNumber > Constants.MAX_NUMBER_OF_SEEKER_THREADS) {
			givenNumber = 200;
			executor = Executors.newFixedThreadPool(200);
		}
		if (givenNumber < 4) {
			givenNumber = 4;
			executor = Executors.newFixedThreadPool(4);
		}
		else {
			numberOfThreads = givenNumber;
			executor = Executors.newFixedThreadPool(numberOfThreads - 3);
		}

		startCrawling();
		EventQueue.invokeLater(new Runnable() {

			public void run() {

				try {
					VerticesFrame vertiPoints_frame = new VerticesFrame();
					vertiPoints_frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}

			}

		});
	}


	public static void stopRequest() throws FileNotFoundException,
			UnsupportedEncodingException {
		// Implement a neat way to stop

		try {
			stopRequested = true;

			executor.shutdown();
			databaseThread.writeAllWorkAtHand();
			executor.awaitTermination(1000, TimeUnit.MICROSECONDS);
			System.exit(0);

		} catch (InterruptedException e) {
			executor.shutdownNow();
		}
		// System.out.println("Exiting normally...");

	}

	public static void startCrawling() throws Exception {

		// Start the simple statistics viewer

		// Read already existing data

		// If there's already work in the queue, read it and start it as well.
		// Otherwise, use some default site
		// A nice one to use is as newspaper, for example http://www.trouw.nl
		stopRequested = false;
		if(selectedWebsite==""){
			selectedWebsite = Constants.DEFAULT_START;
		}
		if (!selectedWebsite.toLowerCase().matches("^\\w+://www.*")) {
			selectedWebsite = "http://" + selectedWebsite;
		}
//		u = new URL(name);
		Vertex vertex1 = new Vertex(selectedWebsite);// ******************
		internetModel.addVertex(vertex1);

		EdgeSeeker edgeSeeker = new EdgeSeeker(internetModel, vertex1,
				executor, databaseThread);
		executor.execute(edgeSeeker);
	}

	@SuppressWarnings("unused")
	public boolean readWorkAtHand() throws Exception {

		boolean result = false;
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(
					model.Constants.WORK_AT_HAND_FILENAME));
			Set<String> cleaning = new HashSet<String>();
			String numberOfNodes = reader.readLine();
			int maxToRead = Integer.parseInt(numberOfNodes);
			for (int i = 0; i < maxToRead; i++) {
				String name = reader.readLine();
				cleaning.add(name);
			}
			for (String name : cleaning) {
				// Insert the stored job into the thread pool here
			}
		} catch (IOException e) {
			// No sweat, just no work at hand. Return false
			result = false;
		}
		if (reader != null) {
			reader.close();
		}

		return result;
	}

	public static void stopCrawling() throws FileNotFoundException,
			UnsupportedEncodingException {
		// Send a stop signal to the thread pool. Make sure to get an overview
		// of all pending jobs
		// (check the API of Executors to see how) and store them in a file for
		// restarting them
		//
		// After that, write the graph to a file and exit the whole application
		executor.shutdown();
		stopRequest();
		internetModel.writeGraph();
		databaseThread.writeAllWorkAtHand();

	}

	/*******************************************************************/
	public static void copyInfo() {
		Vertices.addAll(internetModel.listUpdater());
		copyTopInfo();
	}

	public static void copyTopInfo() {

		ratio = Float.toString(internetModel.getRatioEV());
		vertiTotal = Integer.toString(internetModel.getNumberOfVertices());
		edgesTotal = Integer.toString(internetModel.getNumberOfEdges());
		memUsage = MemoryUtil
				.readableFileSize(internetModel.getBandwidthUsed());
	}
	/**********************************************************************/

	/**
	 * The next class defines the actual panel to show all data on It will only
	 * be used from the frame class, so no seperate file is necessary
	 *
	 * @author harald.drillenburg
	 */

	// ********************************************************************************************************************

	// ********************************************************************************************************************

	// ********************************************************************************************************************

}
