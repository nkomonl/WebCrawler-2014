package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import model.Graph;
import model.Vertex;
import util.URLUtil;

/**
 * This class might form heart and brains of the application. It is a thread
 * which: - is initiated with a Vertex (being an URL) - It is NOT an active
 * object; it runs in a thread pool instead - Takes out all stops in the URL,
 * using URLUtil for this - Then offers new threads in the pool, one for each
 * stop found - And of course it should add a Vertex for every new URL found,
 * and an Edge to reflect the stop.
 * 
 * Remember, a Graph uses a list of unique Vertices, so adding an already
 * existing vertex should not cause duplicates. But neither should it throw
 * exceptions
 * 
 * The thread should also do some optimization by not offering vertices already
 * under examination. Several mechanisms could be used for this. Here, I used a
 * Set to keep track of Vertices already offered for examination. Not the most
 * efficient solution, but good enough for now.
 * 
 * @author harald.drillenburg
 *
 */

public class EdgeSeeker implements Runnable {
	private Graph internetModel;
	private Vertex vertex;
	private ExecutorService executor;
	private DatabaseThread databaseThread;

	public EdgeSeeker(Graph internetModel, Vertex vertex,
			ExecutorService executor, DatabaseThread databaseThread) {
		super();
		this.internetModel = internetModel;
		this.vertex = vertex;
		this.executor = executor;
		this.databaseThread = databaseThread;
	}

	public void storevertex() {
		databaseThread.storeWorkAtHand(vertex);
	}

	public Vertex getvertex() {
		return vertex;
	}

	@Override
	public void run() {
		// The core code goes here. Make it work!

		// Loop through all stops found

		// Create the vertex. If this fails, do nothing with it. Otherwise, add
		// the edge

		// Now the magic follows. For each successfully added target, start
		// another thread to examine it.

		// if executor is in shutdown, don't even bother starting this

		@SuppressWarnings("unused")
		final Lock lock = new ReentrantLock();
		Set<Vertex> testingVertex = Collections
				.synchronizedSet(new HashSet<Vertex>());

		if (!executor.isShutdown()) {
			try {
				List<String> stops = URLUtil.getStops(vertex.getName(),
						internetModel);

				for (String stop : stops) {
					String eraseStop = URLUtil.stripURL(stop);
					if (eraseStop != null) {

						synchronized (new ReentrantLock()) {
							Vertex newVertex = new Vertex(eraseStop);
							boolean present = internetModel.getVertices()
									.contains(newVertex);

							if (!testingVertex.contains(newVertex)) {
								if (!present) {

									internetModel.addVertex(newVertex);
									internetModel.addEdge(vertex, newVertex);

									EdgeSeeker edgeSeeker = new EdgeSeeker(
											internetModel, newVertex, executor,
											databaseThread);
									testingVertex.add(newVertex);

									internetModel.addEdge(vertex, newVertex);

									if (!executor.isShutdown()) {
										executor.execute(edgeSeeker);

									} else {

										internetModel
												.addEdge(vertex, newVertex);

										if (!executor.isShutdown()) {
											executor.execute(edgeSeeker);
										}
									}
								}
							}
						}
					}
				}
				// finished with page
				testingVertex.remove(vertex);
			} catch (IOException exception) {
				exception.printStackTrace();
			}

		} else {

			databaseThread.storeWorkAtHand(vertex);
		}

	}
}